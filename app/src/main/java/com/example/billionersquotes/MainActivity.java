package com.example.billionersquotes;

import android.app.ListActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends ListActivity {
    String[] peoplename = {"Bill Gates", "Mark Zuckerberg","Mark Kuban"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
       // setSupportActionBar(toolbar);
       // peoplename = new String[4];
        //Resources res = getResources();
        //peoplename = res.getStringArray(R.array.billioners);
        setListAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1, peoplename));


    }

    protected void onListItemClick(ListView lv,View v, int position, long id)
    {
        super.onListItemClick(lv, v, position, id);
        String openClass = peoplename[position];
        new AlertDialog.Builder(this)
                .setTitle("Item Selected")
                .setMessage("Selected Item "+openClass)
                .setNeutralButton("OK", null)
                .show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
